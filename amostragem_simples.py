# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 12:01:32 2020

@author: Matheus Glauber R. Jorão (glauber.jordao1995@gmail.com)
"""

#Panda é uma biblioteca usada para trabalhos com Datas Frames ou ciência de dados
import pandas as pd
#Biblioteca com funções estatísticas
import numpy as np

#read lê um arquivo compatível com a função
base = pd.read_csv('iris.csv')
base
#shape retorna a quantidade de linhas e colunas.
base.shape

#seed retorna sempre o mesmo valor da consulta random, para executar testes
np.random.seed(1234)
#random.choice cria uma amostra seguindo os seguintes parâmetros:
'''a = população da amostra, size = tamanho da amostra, replace = repetição ou 
não da população, p = porcentagem de seleção da população. '''
amostra = np.random.choice(a = [0,1], size = 150, replace=True, p=[0.5, 0.5])
amostra
#Exibe a quantidade da população da amostra
len(amostra)

#Exibe a quantidade da população = 1
len(amostra[amostra == 1])
#Exibe a quantidade da população = 2
len(amostra[amostra == 0])
