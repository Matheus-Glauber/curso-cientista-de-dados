# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 17:28:59 2020

@author: Matheus Glauber
"""

import pandas as pd
import numpy as np
#ceil -> Função para arredondamento de valores.
from math import ceil

populacao = 150
amostra = 15
k = ceil(populacao/amostra)

r = np.random.randint(low = 1, high = k + 1, size = 1)

acumulador = r[0]
sorteados = []

for i in range(amostra):
    print(acumulador)
    sorteados.append(acumulador)
    acumulador += k

base = pd.read_csv('iris.csv')
base_final = base.loc[sorteados]