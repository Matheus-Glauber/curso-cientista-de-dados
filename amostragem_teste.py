# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 15:45:39 2020

@author: Matheus Glauber R. Jorão (glauber.jordao1995@gmail.com)
"""

import pandas as pd
from sklearn.model_selection import train_test_split

base_de_dados = pd.read_csv('alunos.csv')
base_de_dados['Gênero'].value_counts()

base_de_dados.shape

X, _, Y, _ = train_test_split(base_de_dados.iloc[:, 0:6], 
                              base_de_dados.iloc[:, 1],
                              test_size = 0.5,
                              stratify = base_de_dados.iloc[:, 1])

Y.value_counts()
